#include <iostream>
#include <iterator>
#include <franka/duration.h>
#include <franka/exception.h>
#include <franka/robot.h>
#include <franka/model.h>

// funcion para pasar array a ostream para que se puedan printear
template <class T, size_t N>
std::ostream& operator<<(std::ostream& ostream, const std::array<T, N>& array) {
  ostream << "[";
  std::copy(array.cbegin(), array.cend() - 1, std::ostream_iterator<T>(ostream, ","));
  std::copy(array.cend() - 1, array.cend(), std::ostream_iterator<T>(ostream));
  ostream << "]";
  return ostream;
}

int main(){

	const std::string robot_ip = "192.168.1.1";
	//crear objeto de la clase robot, que permite crear una conexion con la controladora con la ip seleccionada
	franka::Robot robot(robot_ip, franka::RealtimeConfig::kIgnore);

	int axisN =7;

	/*crear un objeto del tipo robotState que actualiza el estado del robot.
	esta dentro de robot.h pero este llama a la clase RobotState
	devuelve todo lo relacionado a poses del robot, configuraciones del robot,
	medidas del sensor(posicion, torque y velocidad), torques filtradas generadas por fuerzas externas,
	 posicion y velocidad de los motores, errores.
	*/
	franka::RobotState refreshState;
	refreshState = robot.readOnce();
	auto pos = refreshState.q;
	auto tau = refreshState.tau_J;
	auto extTau = refreshState.O_F_ext_hat_K;

	//print la posicion del robot
	std::cout << "Joint positions: "<< std::endl;
	for(int i=0;i<=axisN;i++)
	{
		std::cout << pos[i] << " "<< std::endl;
	}

	std::cout << "Joint torque measured: "<< std::endl;
	for(int i=0;i<=axisN;i++)
	{
		std::cout << tau[i] << " "<< std::endl;
	}

	/*Estimated external wrench (force, torque) acting on stiffness frame, expressed
   * relative to the @ref o-frame "base frame". Forces applied by the robot to the environment are
   * positive, while forces applied by the environment on the robot are negative. Becomes
   * \f$[0,0,0,0,0,0]\f$ when near or in a singularity. See also @ref k-frame "Stiffness frame K".*/
	std::cout << "external tau torque measured: "<< std::endl;
	for(int i=0;i<=5;i++)
	{
		std::cout << extTau[i] << " "<< std::endl;
	}

	/*crea un objeto a partir del modelo del la cinematic y dinamica del robot
	devuelve poses respecto a frames, jacobiano, matriz de inercia, parametros de la dinamica del robot,
	*/
	franka::Model model(robot.loadModel());

    //print de la pose del ultimo frame
	auto pose = model.pose(franka::Frame::kEndEffector, refreshState);
	std::cout << "Pose: " << std::endl;;
    std::cout << pose << std::endl;

    //print jacobiano
    auto jacobianM = model.bodyJacobian(franka::Frame::kEndEffector, refreshState);
    std::cout << "Jacobian: " << std::endl;;
	std::cout << jacobianM << std::endl;

	//print dynamics
	auto coriolisM = model.coriolis(refreshState);
	auto massM = model.mass(refreshState);
	auto gravityM = model.gravity(refreshState);

    std::cout << "coriolisM: " << std::endl;;
	std::cout << coriolisM << std::endl;
    std::cout << "massM: " << std::endl;;
	std::cout << massM << std::endl;
    std::cout << "gravityM: " << std::endl;;
	std::cout << gravityM << std::endl;

	return 0;
}
