################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
/home/mikel/CodeCloud/PandaCtrlImp/common/MathHelper.cpp \
/home/mikel/CodeCloud/PandaCtrlImp/trajectories/Q7TrajGenerator.cpp \
/home/mikel/CodeCloud/PandaCtrlImp/trajectories/TrajHelper.cpp \
../main02MovingRobot.cpp 

CPP_DEPS += \
./MathHelper.d \
./Q7TrajGenerator.d \
./TrajHelper.d \
./main02MovingRobot.d 

OBJS += \
./MathHelper.o \
./Q7TrajGenerator.o \
./TrajHelper.o \
./main02MovingRobot.o 


# Each subdirectory must supply rules for building sources it contributes
MathHelper.o: /home/mikel/CodeCloud/PandaCtrlImp/common/MathHelper.cpp subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++17 -I"/home/mikel/CodeCloud/PandaCtrlImp/eigen-3.4.0" -I"/home/mikel/CodeCloud/PandaCtrlImp/common" -I"/home/mikel/CodeCloud/PandaCtrlImp/trajectories" -I"/home/mikel/CodeCloud/PandaCtrlImp/libfranka" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Q7TrajGenerator.o: /home/mikel/CodeCloud/PandaCtrlImp/trajectories/Q7TrajGenerator.cpp subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++17 -I"/home/mikel/CodeCloud/PandaCtrlImp/eigen-3.4.0" -I"/home/mikel/CodeCloud/PandaCtrlImp/common" -I"/home/mikel/CodeCloud/PandaCtrlImp/trajectories" -I"/home/mikel/CodeCloud/PandaCtrlImp/libfranka" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

TrajHelper.o: /home/mikel/CodeCloud/PandaCtrlImp/trajectories/TrajHelper.cpp subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++17 -I"/home/mikel/CodeCloud/PandaCtrlImp/eigen-3.4.0" -I"/home/mikel/CodeCloud/PandaCtrlImp/common" -I"/home/mikel/CodeCloud/PandaCtrlImp/trajectories" -I"/home/mikel/CodeCloud/PandaCtrlImp/libfranka" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cpp subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++17 -I"/home/mikel/CodeCloud/PandaCtrlImp/eigen-3.4.0" -I"/home/mikel/CodeCloud/PandaCtrlImp/common" -I"/home/mikel/CodeCloud/PandaCtrlImp/trajectories" -I"/home/mikel/CodeCloud/PandaCtrlImp/libfranka" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean--2e-

clean--2e-:
	-$(RM) ./MathHelper.d ./MathHelper.o ./Q7TrajGenerator.d ./Q7TrajGenerator.o ./TrajHelper.d ./TrajHelper.o ./main02MovingRobot.d ./main02MovingRobot.o

.PHONY: clean--2e-

