#include <iostream>
#include <iterator>
#include <franka/duration.h>
#include <franka/exception.h>
#include <franka/robot.h>
#include <franka/model.h>

//includes propios
#include "TrajHelper.h"
#include "MathHelper.h"
#include "Q7TrajGenerator.h"

std::string aString;

int main(int argc, char** argv) {
    const std::string robot_ip = "192.168.1.1";
	const double publish_rate = 30;

	try {
		//crear objeto de la clase robot, que permite crear una conexion con la controladora con la ip seleccionada
		franka::Robot robot(robot_ip,franka::RealtimeConfig::kIgnore);
//		franka::Robot robot(robot_ip,franka::RealtimeConfig::kEnforce);

		//define los limites de torques y fuerzas en el robot
		robot.setCollisionBehavior(
					{{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
					{{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
					{{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
					{{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});
		//crea un objeto del tipo Robot state y recibo datos de los sensores
		franka::RobotState state;
		state = robot.readOnce();

		//inicializar las variables de acuerdo al estado inicial del robot
		std::array<double,7> robotInitPos = state.q;
		std::array<double,7> robotInitVel = state.dq;

		QVector7 qs = MathHelper::array2qv7(robotInitPos);
		QVector7 dqs = MathHelper::array2qv7(robotInitVel);
		QVector7 ddqs =  { 0, 0, 0, 0, 0, 0, 0};

//		QVector7 qsFin =  { 0, 0, 0, -1.59, 0, 1.57, -0};
		QVector7 qsFin =  { -0.78, 0, 0, -1.59, 0, 1.57, -0};
		QVector7 dqsFin = { 0, 0, 0, 0, 0, 0, 0};
		QVector7 ddqsFin = { 0, 0, 0, 0, 0, 0, 0};

		double t0 = 0;
		double tf = 4;
		double sampling_time = 1.0e-3;
		double TrackSamples =tf/sampling_time;

		std::vector<QVector7> torqueToFile;
		std::vector<QVector7> posToFile;
		std::vector<QVector7> velToFile;

		//calcular las trajectorias deseadas
		std::tuple<std::vector<std::array<double, 7>>, std::vector<std::array<double, 7>>, std::vector<std::array<double, 7>>> Trj = Q7TrajGenerator::getQuinticTrajArray(qs, qsFin, dqs, dqsFin,ddqs,ddqsFin,
																	   t0, tf, sampling_time);
		std::vector<std::array<double, 7>> qd = std::get<0>(Trj);

		//cronometro
		std::chrono::steady_clock sc;
		auto start = sc.now();

		//print pos inicial
		std::cout << "Joint init positions: "<< std::endl;
		for(int i=0;i<=6;i++)
		{
			std::cout << qs[i] << " "<< std::endl;
		}
		std::cout << std::endl << "press intro to continue..." << std::endl;
		std::getline(std::cin,aString);

		// crea el lazo de control del robot, basicamente ejecuta este control de posicion hasta que devuelve MotionFinished
		robot.control([&qd, &TrackSamples,&torqueToFile, &posToFile, &velToFile](const franka::RobotState& robot_state/* state */, franka::Duration /* period */) -> franka::JointPositions
					{
				static unsigned int iter = 0;
				//mientras iter sea mas pequeño que toda la longitud de la trayectoria,
				//crea un objeto del tipo JointPositions y actualiza la trayectoria deseada del robot
				if (iter < TrackSamples) {
					franka::JointPositions posCommand = {
						{qd[iter][0], qd[iter][1], qd[iter][2], qd[iter][3], qd[iter][4], qd[iter][5], qd[iter][6]}
					};

					posToFile.push_back(MathHelper::array2qv7(robot_state.q));
					velToFile.push_back(MathHelper::array2qv7(robot_state.dq));
				    torqueToFile.push_back(MathHelper::array2qv7(robot_state.tau_J));

					++iter;
					return posCommand;
				} else
				{

					//cuando termina la trayectoria, guardar la ultima posicion y llamar MotionFinished para terminar el lazo
					franka::JointPositions lastPosCommand = {
								{qd[TrackSamples-1][0], qd[TrackSamples-1][1], qd[TrackSamples-1][2],
								 qd[TrackSamples-1][3], qd[TrackSamples-1][4], qd[TrackSamples-1][5],
								 qd[TrackSamples-1][6]}
							};
					std::cout << "Robot Final Position: \n";
					for (int i = 0; i < 7; ++i) {
						std::cout << lastPosCommand.q[i] << " " << std::endl;;
					}
					std::cout << std::endl;
					return franka::MotionFinished(lastPosCommand);
				}
			});
		TrajHelper::confDataToFile("DataToFile.txt", torqueToFile, posToFile, velToFile, false);

		auto end = sc.now();
		auto cycleTime = static_cast<std::chrono::duration < double >> (end - start);
		std::cout << std::endl << "each cycle time was: " << cycleTime.count() / TrackSamples << std::endl;
		std::cout << std::endl << "total cycle time was: " << cycleTime.count() << std::endl;
		}
		catch (const franka::ControlException& e)
			{
				std::cerr << "Control exception: " << e.what() << std::endl;
				return -1;
			}

	return 0;
}
