#include <iostream>
#include <iterator>
#include <franka/duration.h>
#include <franka/exception.h>
#include <franka/robot.h>
#include <franka/model.h>

// Includes for custom utility functions and classes
#include "TrajHelper.h"
#include "MathHelper.h"
#include "Q7TrajGenerator.h"
#include "ImpCtrlSet.h"

// Function to convert an array to an ostream for printing
template <class T, size_t N>
std::ostream& operator<<(std::ostream& ostream, const std::array<T, N>& array) {
  // Print the contents of the array with proper formatting
  ostream << "[";
  std::copy(array.cbegin(), array.cend() - 1, std::ostream_iterator<T>(ostream, ","));
  std::copy(array.cend() - 1, array.cend(), std::ostream_iterator<T>(ostream));
  ostream << "]";
  return ostream;
}

std::string aString;

int main(int argc, char** argv) {
    // Configuration constants
    const std::string robot_ip = "192.168.1.1";

    // Lambda function for torque control
    std::function<franka::Torques(const franka::RobotState&, franka::Duration)> ImpCtrl;

    try {
        // Create a connection to the robot controller with the specified IP
        franka::Robot robot(robot_ip, franka::RealtimeConfig::kEnforce);

        // Set collision behavior for the robot
        robot.setCollisionBehavior(
            {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
            {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
            {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
            {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});

        // Create an object of the RobotState class to receive sensor data
        franka::RobotState state;
        state = robot.readOnce();

        // Create an instance of ImpCtrlSet for control
	    ImpCtrlSet Ctrl;

        // Initialize variables to store joint torques
        std::array<double, 7> tau = state.tau_J;

        // Initial message and control setup
        std::cout << std::endl << "Press Enter to continue..." << std::endl;
        std::getline(std::cin, aString);

        std::cout << std::endl << "Press Enter to finish the motion loop" << std::endl;
//         Control the robot using the ImpCtrlSet class
		robot.control([=, &Ctrl](const franka::RobotState& robot_state, franka::Duration period) -> franka::Torques {
			return Ctrl.ExecuteControlLoop(robot_state, period);
		});
    }
    catch (const franka::ControlException& e) {
        // Handle control exceptions and print error messages
        std::cerr << "Control exception: " << e.what() << std::endl;
        return -1;
    }

    return 0;
}
