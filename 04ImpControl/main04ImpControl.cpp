#include <iostream>
#include <iterator>
#include <franka/duration.h>
#include <franka/exception.h>
#include <franka/robot.h>
#include <franka/model.h>

// Includes for custom utility functions and classes
#include "TrajHelper.h"
#include "MathHelper.h"
#include "Q7TrajGenerator.h"

// Function to convert an array to an ostream for printing
template <class T, size_t N>
std::ostream& operator<<(std::ostream& ostream, const std::array<T, N>& array) {
  // Print the contents of the array with proper formatting
  ostream << "[";
  std::copy(array.cbegin(), array.cend() - 1, std::ostream_iterator<T>(ostream, ","));
  std::copy(array.cend() - 1, array.cend(), std::ostream_iterator<T>(ostream));
  ostream << "]";
  return ostream;
}

std::string aString;

int main(int argc, char** argv) {
    // Configuration constants
    const std::string robot_ip = "192.168.1.1";
    const double publish_rate = 30;

    // Lambda function for torque control
    std::function<franka::Torques(const franka::RobotState&, franka::Duration)> ImpCtrl;

    try {
        // Create a connection to the robot controller with the specified IP
        franka::Robot robot(robot_ip, franka::RealtimeConfig::kEnforce);

        // Set collision behavior for the robot
        robot.setCollisionBehavior(
            {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
            {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
            {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
            {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});

        // Create an object of the RobotState class to receive sensor data
        franka::RobotState state;
        state = robot.readOnce();

        // Initialize variables based on the robot's initial state
        std::array<double, 7> robotInitPos = state.q;
        std::array<double, 7> robotInitVel = state.dq;
        std::array<double, 7> tau = state.tau_J;

        // Convert initial positions to custom data structures
        QVector7 qs = MathHelper::array2qv7(robotInitPos);
        QVector7 dqs = MathHelper::array2qv7(robotInitVel);
        QVector7 ddqs = {0, 0, 0, 0, 0, 0, 0};

        // Define desired final poses
        QVector7 qsFin = {-0.78, 0, 0, -1.59, 0, 1.57, 0};
        QVector7 dqsFin = {0, 0, 0, 0, 0, 0, 0};
        QVector7 ddqsFin = {0, 0, 0, 0, 0, 0, 0};

        // Time parameters for trajectory generation
        double t0 = 0;
        double tf = 4;
        double sampling_time = 2.0e-3;
        double TrackSamples = tf / sampling_time;

        // Containers for storing torque, position, and velocity data
        std::vector<QVector7> torqueToFile;
        std::vector<QVector7> posToFile;
        std::vector<QVector7> velToFile;

        // Generate desired trajectories
        std::tuple<std::vector<std::array<double, 7>>, std::vector<std::array<double, 7>>, std::vector<std::array<double, 7>>> Trj =
            Q7TrajGenerator::getQuinticTrajArray(qs, qsFin, dqs, dqsFin, ddqs, ddqsFin, t0, tf, sampling_time);

        // Initial message and control setup
        std::cout << std::endl << "Press Enter to finish motion" << std::endl;
        ImpCtrl = [=, &tau](const franka::RobotState& robot_state, franka::Duration period) -> franka::Torques {
            // Read actual joint torques from the robot_state
            franka::Torques actualTorques = robot_state.tau_J;

            // Your torque control logic here
            // Here, the desired torque is set to be the same as the actual torque (no control action).
            franka::Torques desiredTorques = actualTorques;

            // Check for user input to finish the motion
            if (std::getline(std::cin, aString)) {
                std::cout << std::endl << "Finished motion, shutting down example" << std::endl;
                return franka::MotionFinished(desiredTorques);
            }

            return desiredTorques;
        };

        // Start a timer to measure execution time
        std::chrono::steady_clock sc;
        auto start = sc.now();

        // Print initial joint positions
        std::cout << "Joint init positions: " << std::endl;
        for (int i = 0; i <= 6; i++) {
            std::cout << qs[i] << " " << std::endl;
        }

        std::cout << std::endl << "Press Enter to continue..." << std::endl;
        std::getline(std::cin, aString);

        // Control the robot using the ImpCtrl lambda function
        robot.control(ImpCtrl);
    }
    catch (const franka::ControlException& e) {
        // Handle control exceptions and print error messages
        std::cerr << "Control exception: " << e.what() << std::endl;
        return -1;
    }

    return 0;
}
