#include "ImpCtrlSet.h"
#include <franka/duration.h>
#include <franka/exception.h>
#include <franka/robot.h>
#include <franka/model.h>
#include <iostream>

std::string finish;

ImpCtrlSet::ImpCtrlSet() {
}

franka::Torques ImpCtrlSet::ExecuteControlLoop(const franka::RobotState& robot_state, franka::Duration period)
{

        // Read actual joint torques from the robot_state
        franka::Torques actualTorques = robot_state.tau_J;

        // Your torque control logic here
        // Here, the desired torque is set to be the same as the actual torque (no control action).
        franka::Torques desiredTorques = actualTorques;

        // Check for user input to finish the motion
        if (std::getline(std::cin, finish)) {
            std::cout << std::endl << "Finished motion, shutting down example" << std::endl;
            return franka::MotionFinished(desiredTorques);
        }

        return desiredTorques;
    }

ImpCtrlSet::~ImpCtrlSet() {
}

