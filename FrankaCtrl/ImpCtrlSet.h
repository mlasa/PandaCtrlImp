#ifndef IMPCTRLSET_H_
#define IMPCTRLSET_H_

#include <franka/robot.h>

class ImpCtrlSet {
public:
	ImpCtrlSet();
	virtual ~ImpCtrlSet();

	 franka::Torques ExecuteControlLoop(const franka::RobotState& robot_state, franka::Duration period);

};

#endif /* IMPCTRLSET_H_ */
