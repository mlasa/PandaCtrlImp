#include "MathHelper.h"
#include <array>

std::default_random_engine MathHelper::eng(time(0));

double MathHelper::random(double min, double max) //range : [min, max]
{
  std::uniform_real_distribution<double> unif(min,max);
  double u = unif(MathHelper::eng);
  return u;
}

QVector6 MathHelper::stdV2qv6(const std::vector<double>& v)
{
  QVector6 qv6;

  for(int i=0; i<6;i++)  //qv6.rows()
  {
    qv6[i]=v[i];
  }
  return qv6;
}

QVector7 MathHelper::array2qv7(const std::array<double,7>& v)
{
  QVector7 qv7;

  for(int i=0; i<7;i++)  //qv6.rows()
  {
    qv7[i]=v[i];
  }
  return qv7;
}

std::array<double,7> MathHelper::qv7Array(const QVector7& v)
{
	std::array<double,7> qv7;

  for(int i=0; i<7;i++)  //qv6.rows()
  {
    qv7[i]=v[i];
  }
  return qv7;
}

std::vector<double> MathHelper::q6v2stdV(const QVector6& qv)
{
  std::vector<double> v;

  for(int i=0; i< 6;i++)
  {
     v.push_back(qv(i));
  }
  return v;
}







