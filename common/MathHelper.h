
#ifndef MATHHELPER_H_
#define MATHHELPER_H_

#include <random>
#include <array>
#include <vector>

#include "TypeDefinitions.h"

using namespace ourTypeDefinitions;


class MathHelper
{
  protected:
    static std::default_random_engine eng;
  public:
    static void initialize();
    static double random(double min, double max); //range : [min, max]
    static QVector6 stdV2qv6(const std::vector<double>&);
    static std::vector<double> q6v2stdV(const QVector6& qv6);
    static QVector7 array2qv7(const std::array<double,7>&);
    static std::array<double,7> qv7Array(const QVector7& v);
    static QVector6 deg2rad(const QVector6& qv6) {return M_PI/180*qv6;};
    static QVector6 rad2deg(const QVector6& qv6) {return 180/M_PI*qv6;};
    static double rad2deg(const double rads) {return 180/M_PI*rads;};
    static double deg2rad(const double degs) {return M_PI/180*degs;};

};

#endif /* MATHHELPER_H_ */
