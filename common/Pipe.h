
#ifndef PIPE_H_
#define PIPE_H_

template<typename T, unsigned int BUFFER_SIZE>
class Pipe
{
  protected:
    T theArray[BUFFER_SIZE];
    unsigned int head, tail, nElements;

  public:
    Pipe(): head(0), tail(0), nElements(0) {}
    void put(T elem);
    T get();
    unsigned int howManyElements() {return nElements;}
    unsigned int howManyHoles() {return BUFFER_SIZE - nElements-1;}
};

template<typename T, unsigned int BUFFER_SIZE>
void Pipe<T, BUFFER_SIZE>::put(T elem)
{
  theArray[head]=elem;
  head = (head+1) % BUFFER_SIZE;
  nElements = nElements +1;
}

template<typename T, unsigned int BUFFER_SIZE>
T Pipe<T, BUFFER_SIZE>::get()
{
  T x = theArray[tail];
  nElements = nElements - 1;
  tail = (tail+1) % BUFFER_SIZE;
  return x;
}

#endif /* PIPE_H_ */
