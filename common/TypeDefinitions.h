
#ifndef TYPEDEFINITIONS_H_
#define TYPEDEFINITIONS_H_

#include "Eigen/Dense"


namespace ourTypeDefinitions
{

  using QVector6=Eigen::Vector<double, 6>;
  using QVector7=Eigen::Vector<double, 7>;
}
#endif
