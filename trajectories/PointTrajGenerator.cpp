#include "PointTrajGenerator.h"

//#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <vector>
#include "Eigen/Dense"
#include "TypeDefinitions.h"

using namespace std;
using namespace Eigen;

using namespace ourTypeDefinitions;


std::vector<QVector6> PointTrajGenerator::loadTrajectories(const char* fileName, double sampling_time, double t_0, double t_f)
{
  std::vector<QVector6> confTrj;

  std::vector<std::string> row;
  std::string line, word;

  std::ifstream file (fileName);

  if(file.is_open())
  {
     while(getline(file, line))
     {
    	 QVector6 conf;
       row.clear();
       std::stringstream str(line);

       for(int i=0; i<6; i++)
       {
         getline(str, word, ',');
         conf[i] = std::stod(word);
         //std::cout<<conf[i]<<" ";
       }
       confTrj.push_back(conf);
       //std::cout<<std::endl;
     }
  }
  return confTrj;
}


std::vector<QVector6> PointTrajGenerator::getCubicTraj(QVector6 qv6Ini, QVector6 qv6Fin,  QVector6 dqv6Ini,
                                          QVector6 dqv6Fin, double t_0, double t_f, double sampling_time)
{
  std::vector<std::vector<double>> individualTrajs;
  std::vector<QVector6> qv6Traj;

  for(int i=0;i<6;i++)
  {
    individualTrajs.push_back(getCubicTraj(qv6Ini[i], qv6Fin[i], dqv6Ini[i], dqv6Fin[i], t_0, t_f, sampling_time, false));
  }
  for(unsigned long int i=0;i<individualTrajs[0].size();i++)
  {
    QVector6 point={individualTrajs[0][i], individualTrajs[1][i], individualTrajs[2][i],
                    individualTrajs[3][i], individualTrajs[4][i], individualTrajs[5][i]};
    qv6Traj.push_back({point});
  }
  return qv6Traj;
}


std::tuple <std::vector<QVector6> , std::vector<QVector6> , std::vector<QVector6>> PointTrajGenerator::getQuinticTraj(QVector6 qv6Ini, QVector6 qv6Fin,  QVector6 dqv6Ini,
                                          QVector6 dqv6Fin, QVector6 ddqv6Ini, QVector6 ddqv6Fin, double t_0,
										  double t_f, double sampling_time)
{
	std::vector<std::vector<double>> individualQ(6);
	std::vector<std::vector<double>> individualdQ(6);
	std::vector<std::vector<double>> individualddQ(6);

	std::vector<QVector6> qv6Traj;
	std::vector<QVector6> dqv6Traj;
	std::vector<QVector6> ddqv6Traj;

	 for (int i = 0; i < 6; i++)
		{
		 	std::tuple <std::vector<double>, std::vector<double>, std::vector<double>> AllTraj =
					  getQuinticTraj(qv6Ini[i], qv6Fin[i], dqv6Ini[i], dqv6Fin[i], ddqv6Ini[i], ddqv6Fin[i], t_0, t_f, sampling_time, false);
			individualQ[i] = std::get<0>(AllTraj);
			individualdQ[i] = std::get<1>(AllTraj);
			individualddQ[i]= std::get<2>(AllTraj);
		}

	 	  //posicion
	 	  for(unsigned long int i=0;i<individualQ[0].size();i++)
	 	  {
	 	    QVector6 point={individualQ[0][i], individualQ[1][i], individualQ[2][i],
	 	    				individualQ[3][i], individualQ[4][i], individualQ[5][i]};
	 	    qv6Traj.push_back({point});
	 	  }

	 	  //velocidad
	 	  for(unsigned long int i=0;i<individualdQ[0].size();i++)
	 	  {
	 	     QVector6 point={individualdQ[0][i], individualdQ[1][i], individualdQ[2][i],
	 	    		 	 	 individualdQ[3][i], individualdQ[4][i], individualdQ[5][i]};
	 	     dqv6Traj.push_back({point});
	 	  }

	 	  //aceleracion
	 	  for(unsigned long int i=0;i<individualddQ[0].size();i++)
	 	  {
	 	     QVector6 point={individualddQ[0][i], individualddQ[1][i], individualddQ[2][i],
	 	    		 	 	 individualddQ[3][i], individualddQ[4][i], individualddQ[5][i]};
	 	     ddqv6Traj.push_back({point});
	 	   }
	 	   return std::make_tuple(qv6Traj, dqv6Traj, ddqv6Traj);
	 }


std::vector<double> PointTrajGenerator::getCubicTraj(double q_0, double q_f, double dq_0, double dq_f, double t_0, double t_f, double sampling_time, bool DEBUG)
{
  Matrix4d A;
  Vector4d b;

  int n_time = ceil((t_f - t_0) / sampling_time);
  //cout << n_time << endl;
  float time = n_time;

  A << 1, t_0, pow(t_0, 2), pow(t_0, 3),
    0, 1, 2 * t_0, 3 * pow(t_0, 2),
    1, t_f, pow(t_f, 2), pow(t_f, 3),
    0, 1, 2 * t_f, 3 * pow(t_f, 2),

    b << q_0, dq_0, q_f, dq_f;

  VectorXd a = A.colPivHouseholderQr().solve(b);


  RowVectorXd  q_traj(n_time+1);
  RowVectorXd  dq_traj(n_time+1);
  //q_traj(1) < 10;
  int cont = 0;
  for (int i = 0; i <= n_time; i++)
  {
    cont = cont + 1;
    time = t_0 + i * sampling_time;
    //std::cout << "a:\n" << a(0) + a(1) * time + a(2) * pow(time, 2) + a(3) * pow(time, 3) << std::endl;
    q_traj(i) = a(0) + a(1) * time + a(2) * pow(time, 2) + a(3) * pow(time, 3);
    //q_traj(0)  = a(0) + a(1) * time + a(2) * pow(time, 2) + a(3) * pow(time, 3);
    dq_traj(i) = a(1) + 2 * a(2) * time + 3 * a(3) * pow(time, 2);
  }
  std::vector<double>  q_tr = std::vector<double>(q_traj.data(), q_traj.data() + q_traj.size());
  //dq_tr=std::vector<double>(dq_traj.data(), dq_traj.data() + dq_traj.size());


  if (DEBUG)
  {
    std::cout << "Here is the matrix A:\n" << A << std::endl;
    std::cout << "Here is the vector b:\n" << b << std::endl;
    std::cout << "The solution is:\n" << a << std::endl;

    std::cout << "The traj is:\n" << q_traj << std::endl;
    std::cout << "The dtraj is:\n" << dq_traj << std::endl;
  }

  return q_tr;
}

std::tuple <std::vector<double>, std::vector<double>, std::vector<double>> PointTrajGenerator::getQuinticTraj(double q_0, double q_f,
																			double dq_0, double dq_f, double ddq_0, double ddq_f, double t_0,
																			double t_f, double samplingTime, double DEBUG)

{
	MatrixXd A(6, 6);
	VectorXd b(6);

	int n_time = ceil((t_f - t_0) / samplingTime);
	float time = n_time;


	A << 1, t_0, pow(t_0, 2), pow(t_0, 3), pow(t_0, 4), pow(t_0, 5),
		0, 1, 2 * t_0, 3 * pow(t_0, 2), 4 * pow(t_0, 3), 5 * pow(t_0, 4),
		0, 0, 2, 6 * t_0, 12 * pow(t_0, 2), 20 * pow(t_0, 3),
		1, t_f, pow(t_f, 2), pow(t_f, 3), pow(t_f, 4), pow(t_f, 5),
		0, 1, 2 * t_f, 3 * pow(t_f, 2), 4 * pow(t_f, 3), 5 * pow(t_f, 4),
		0, 0, 2, 6 * t_f, 12 * pow(t_f, 2), 20 * pow(t_f, 3),

		b << q_0, dq_0, ddq_0, q_f, dq_f, ddq_f;

	VectorXd a = A.colPivHouseholderQr().solve(b);


	RowVectorXd  q_traj(n_time);
	RowVectorXd  dq_traj(n_time);
	RowVectorXd  ddq_traj(n_time);

	int cont = 0;
	for (int i = 0; i < n_time; i++)
	{
		cont = cont + 1;
		time = t_0 + i * samplingTime;

		q_traj(i) = a(0) + a(1) * time + a(2) * pow(time, 2) + a(3) * pow(time, 3) + a(4) * pow(time, 4) + a(5) * pow(time, 5);
		dq_traj(i) = a(1) + 2 * a(2) * time + 3 * a(3) * pow(time, 2) + 4 * a(4) * pow(time, 3) + 5 * a(5) * pow(time, 4);
		ddq_traj(i) = 2 * a(2) + 6 * a(3) * time + 12 * a(4) * pow(time, 2) + 20 * a(5) * pow(time, 3);

	}

	std::vector<double> q_tr(q_traj.data(), q_traj.data() + q_traj.size());
	std::vector<double> dq_tr(dq_traj.data(), dq_traj.data() + dq_traj.size());
	std::vector<double> ddq_tr(ddq_traj.data(), ddq_traj.data() + ddq_traj.size());

	if (DEBUG)
	{
		std::cout << "Here is the matrix A:\n" << A << std::endl;
		std::cout << "Here is the vector b:\n" << b << std::endl;
		std::cout << "The solution is:\n" << a << std::endl;

		std::cout << "The traj is:\n" << q_traj << std::endl;
		std::cout << "The dtraj is:\n" << dq_traj << std::endl;
	}

	return std::make_tuple(q_tr, dq_tr, ddq_tr);
}



