#pragma once
#include <vector>
#include "TypeDefinitions.h"

using namespace ourTypeDefinitions;

class PointTrajGenerator
{
  protected:
    static std::vector<double> getCubicTraj(double q_0, double q_f, double dq_0,
                                              double dq_f, double t_0, double t_f, double sampling_time, bool DEBUG);
    static std::tuple <std::vector<double>, std::vector<double>, std::vector<double>> getQuinticTraj(double q_0, double q_f,
			double dq_0, double dq_f, double ddq_0, double ddq_f, double t_0,
			double t_f, double samplingTime, double DEBUG);
  public:


    static std::vector<QVector6> getCubicTraj(QVector6 qv6Ini, QVector6 qv6Fin,  QVector6 dqv6Ini,
                                              QVector6 dqv6Fin, double t_0, double t_f, double sampling_time);

    static std::vector<QVector6>  loadTrajectories(const char * fileName, double sampling_time, double t_0, double t_f);

    static std::tuple <std::vector<QVector6> , std::vector<QVector6> , std::vector<QVector6>> getQuinticTraj(QVector6 qv6Ini, QVector6 qv6Fin,  QVector6 dqv6Ini,
            QVector6 dqv6Fin, QVector6 ddqv6Ini, QVector6 ddqv6Fin, double t_0,
			  double t_f, double sampling_time);
};

