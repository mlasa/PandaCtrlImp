#include "Q7TrajGenerator.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "Eigen/Dense"
#include "TypeDefinitions.h"

using namespace std;
using namespace Eigen;

using namespace ourTypeDefinitions;


std::vector<QVector7> Q7TrajGenerator::loadTrajectories(const char* fileName, double sampling_time, double t_0, double t_f)
{
  std::vector<QVector7> confTrj;

  std::vector<std::string> row;
  std::string line, word;

  std::ifstream file (fileName);

  if(file.is_open())
  {
     while(getline(file, line))
     {
    	 QVector7 conf;
       row.clear();
       std::stringstream str(line);

       for(int i=0; i<7; i++)
       {
         getline(str, word, ',');
         conf[i] = std::stod(word);
         //std::cout<<conf[i]<<" ";
       }
       confTrj.push_back(conf);
       //std::cout<<std::endl;
     }
  }
  return confTrj;
}

std::tuple <std::vector<QVector7> , std::vector<QVector7> , std::vector<QVector7>> Q7TrajGenerator::getQuinticTraj(QVector7 qv7Ini, QVector7 qv7Fin,  QVector7 dqv7Ini,
		QVector7 dqv7Fin, QVector7 ddqv7Ini, QVector7 ddqv7Fin, double t_0,
										  double t_f, double sampling_time)
{
	std::vector<std::vector<double>> individualQ(7);
	std::vector<std::vector<double>> individualdQ(7);
	std::vector<std::vector<double>> individualddQ(7);

	std::vector<QVector7> qv7Traj;
	std::vector<QVector7> dqv7Traj;
	std::vector<QVector7> ddqv7Traj;

	 for (int i = 0; i < 7; i++)
		{
		 	std::tuple <std::vector<double>, std::vector<double>, std::vector<double>> AllTraj =
					  getQuinticTraj(qv7Ini[i], qv7Fin[i], dqv7Ini[i], dqv7Fin[i], ddqv7Ini[i], ddqv7Fin[i], t_0, t_f, sampling_time, false);
			individualQ[i] = std::get<0>(AllTraj);
			individualdQ[i] = std::get<1>(AllTraj);
			individualddQ[i]= std::get<2>(AllTraj);
		}

	 	  //posicion
	 	  for(unsigned long int i=0;i<individualQ[0].size();i++)
	 	  {
	 		 QVector7 point={individualQ[0][i], individualQ[1][i], individualQ[2][i],
	 	    				individualQ[3][i], individualQ[4][i], individualQ[5][i],individualQ[6][i]};
	 	    qv7Traj.push_back({point});
	 	  }

	 	  //velocidad
	 	  for(unsigned long int i=0;i<individualdQ[0].size();i++)
	 	  {
	 		 QVector7 point={individualdQ[0][i], individualdQ[1][i], individualdQ[2][i],
	 	    		 	 	 individualdQ[3][i], individualdQ[4][i], individualdQ[5][i],individualQ[6][i]};
	 	     dqv7Traj.push_back({point});
	 	  }

	 	  //aceleracion
	 	  for(unsigned long int i=0;i<individualddQ[0].size();i++)
	 	  {
	 		 QVector7 point={individualddQ[0][i], individualddQ[1][i], individualddQ[2][i],
	 	    		 	 	 individualddQ[3][i], individualddQ[4][i], individualddQ[5][i],individualQ[6][i]};
	 	     ddqv7Traj.push_back({point});
	 	   }
	 	   return std::make_tuple(qv7Traj, dqv7Traj, ddqv7Traj);
	 }


std::tuple<std::vector<std::array<double, 7>>, std::vector<std::array<double, 7>>, std::vector<std::array<double, 7>>>
Q7TrajGenerator::getQuinticTrajArray(QVector7 qv7Ini, QVector7 qv7Fin, QVector7 dqv7Ini,
                   QVector7 dqv7Fin, QVector7 ddqv7Ini, QVector7 ddqv7Fin, double t_0,
                   double t_f, double sampling_time)
{
    std::vector<std::vector<double>> individualQ(7);
    std::vector<std::vector<double>> individualdQ(7);
    std::vector<std::vector<double>> individualddQ(7);

    std::vector<std::array<double, 7>> qv7Traj;
    std::vector<std::array<double, 7>> dqv7Traj;
    std::vector<std::array<double, 7>> ddqv7Traj;

    for (int i = 0; i < 7; i++)
    {
        std::tuple<std::vector<double>, std::vector<double>, std::vector<double>> AllTraj =
            getQuinticTraj(qv7Ini[i], qv7Fin[i], dqv7Ini[i], dqv7Fin[i], ddqv7Ini[i], ddqv7Fin[i], t_0, t_f, sampling_time, false);
        individualQ[i] = std::get<0>(AllTraj);
        individualdQ[i] = std::get<1>(AllTraj);
        individualddQ[i] = std::get<2>(AllTraj);
    }

    // Position
    for (unsigned long int i = 0; i < individualQ[0].size(); i++)
    {
        std::array<double, 7> point;
        for (int j = 0; j < 7; j++)
        {
            point[j] = individualQ[j][i];
        }
        qv7Traj.push_back(point);
    }

    // Velocity
    for (unsigned long int i = 0; i < individualdQ[0].size(); i++)
    {
        std::array<double, 7> point;
        for (int j = 0; j < 7; j++)
        {
            point[j] = individualdQ[j][i];
        }
        dqv7Traj.push_back(point);
    }

    // Acceleration
    for (unsigned long int i = 0; i < individualddQ[0].size(); i++)
    {
        std::array<double, 7> point;
        for (int j = 0; j < 7; j++)
        {
            point[j] = individualddQ[j][i];
        }
        ddqv7Traj.push_back(point);
    }
    return std::make_tuple(qv7Traj, dqv7Traj, ddqv7Traj);
}

std::tuple <std::vector<double>, std::vector<double>, std::vector<double>> Q7TrajGenerator::getQuinticTraj(double q_0, double q_f,
																			double dq_0, double dq_f, double ddq_0, double ddq_f, double t_0,
																			double t_f, double samplingTime, double DEBUG)

{
	MatrixXd A(6, 6);
	VectorXd b(6);

	int n_time = ceil((t_f - t_0) / samplingTime);
	float time = n_time;


	A << 1, t_0, pow(t_0, 2), pow(t_0, 3), pow(t_0, 4), pow(t_0, 5),
		0, 1, 2 * t_0, 3 * pow(t_0, 2), 4 * pow(t_0, 3), 5 * pow(t_0, 4),
		0, 0, 2, 6 * t_0, 12 * pow(t_0, 2), 20 * pow(t_0, 3),
		1, t_f, pow(t_f, 2), pow(t_f, 3), pow(t_f, 4), pow(t_f, 5),
		0, 1, 2 * t_f, 3 * pow(t_f, 2), 4 * pow(t_f, 3), 5 * pow(t_f, 4),
		0, 0, 2, 6 * t_f, 12 * pow(t_f, 2), 20 * pow(t_f, 3),

		b << q_0, dq_0, ddq_0, q_f, dq_f, ddq_f;

	VectorXd a = A.colPivHouseholderQr().solve(b);


	RowVectorXd  q_traj(n_time);
	RowVectorXd  dq_traj(n_time);
	RowVectorXd  ddq_traj(n_time);

	int cont = 0;
	for (int i = 0; i < n_time; i++)
	{
		cont = cont + 1;
		time = t_0 + i * samplingTime;

		q_traj(i) = a(0) + a(1) * time + a(2) * pow(time, 2) + a(3) * pow(time, 3) + a(4) * pow(time, 4) + a(5) * pow(time, 5);
		dq_traj(i) = a(1) + 2 * a(2) * time + 3 * a(3) * pow(time, 2) + 4 * a(4) * pow(time, 3) + 5 * a(5) * pow(time, 4);
		ddq_traj(i) = 2 * a(2) + 6 * a(3) * time + 12 * a(4) * pow(time, 2) + 20 * a(5) * pow(time, 3);

	}

	std::vector<double> q_tr(q_traj.data(), q_traj.data() + q_traj.size());
	std::vector<double> dq_tr(dq_traj.data(), dq_traj.data() + dq_traj.size());
	std::vector<double> ddq_tr(ddq_traj.data(), ddq_traj.data() + ddq_traj.size());

	if (DEBUG)
	{
		std::cout << "Here is the matrix A:\n" << A << std::endl;
		std::cout << "Here is the vector b:\n" << b << std::endl;
		std::cout << "The solution is:\n" << a << std::endl;

		std::cout << "The traj is:\n" << q_traj << std::endl;
		std::cout << "The dtraj is:\n" << dq_traj << std::endl;
	}

	return std::make_tuple(q_tr, dq_tr, ddq_tr);
}
