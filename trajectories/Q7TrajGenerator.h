#pragma once

#ifndef Q7TRAJGENERATOR_H_
#define Q7TRAJGENERATOR_H_

#include <vector>
#include "TypeDefinitions.h"

using namespace ourTypeDefinitions;

class Q7TrajGenerator {

protected:
	static std::tuple <std::vector<double>, std::vector<double>, std::vector<double>> getQuinticTraj(double q_0, double q_f,
				double dq_0, double dq_f, double ddq_0, double ddq_f, double t_0,
				double t_f, double samplingTime, double DEBUG);
public:
	static std::vector<QVector7>  loadTrajectories(const char * fileName, double sampling_time, double t_0, double t_f);

	static std::tuple <std::vector<QVector7> , std::vector<QVector7> , std::vector<QVector7>> getQuinticTraj(QVector7 qv7Ini, QVector7 qv7Fin,  QVector7 dqv7Ini,
	    		QVector7 dqv7Fin, QVector7 ddqv7Ini, QVector7 ddqv7Fin, double t_0,
				double t_f, double sampling_time);

	static std::tuple<std::vector<std::array<double, 7>>, std::vector<std::array<double, 7>>, std::vector<std::array<double, 7>>>
	getQuinticTrajArray(QVector7 qv7Ini, QVector7 qv7Fin, QVector7 dqv7Ini,
                   QVector7 dqv7Fin, QVector7 ddqv7Ini, QVector7 ddqv7Fin, double t_0,
                   double t_f, double sampling_time);
};

#endif /* Q7TRAJGENERATOR_H_ */
