#include <fstream>
#include "TrajHelper.h"
#include "MathHelper.h"

void TrajHelper::confVectorToFile(const char* fileName, const std::vector<QVector7> &thePoints, bool toDegrees)
{
  std::ofstream outfile = std::ofstream(fileName, std::ios_base::out);

  for (unsigned long int i=0; i<thePoints.size(); i++)
  {
    std::string floatString="";
    std::string pointString="";
    QVector7 v=thePoints[i];
    for (unsigned long int j=0; j< 7; j++)
    {
      floatString.append(std::to_string(v[j]));
      if (j<6)
         floatString.append(", ");
    }
    outfile << floatString<< std::endl;
  }
}

void TrajHelper::confDataToFile(const char* fileName, const std::vector<QVector7>& torques, const std::vector<QVector7>& posPoints, const std::vector<QVector7>& velPoints, bool toDegrees)
{
  std::ofstream outfile = std::ofstream(fileName, std::ios_base::out);

  for (unsigned long int i = 0; i < torques.size(); i++)
  {
    std::string floatString = "";
    std::string pointString = "";

    QVector7 t = torques[i];
    QVector7 p = posPoints[i];
    QVector7 v = velPoints[i];
    for (unsigned long int j = 0; j < 7; j++)
    {
      floatString.append(std::to_string(t[j]));
      if (j < 6)
        floatString.append(", ");
    }
    for (unsigned long int jj = 0; jj < 7; jj++)
    {
      floatString.append(std::to_string(p[jj]));
      if (jj < 6)
        floatString.append(", ");
    }
    for (unsigned long int jjj = 0; jjj < 7; jjj++)
    {
      floatString.append(std::to_string(v[jjj]));
      if (jjj < 6)
        floatString.append(", ");
    }
    outfile << floatString << std::endl;
  }
}



//void TrajHelper::rad2deg(std::vector<QVector7> &traj)
//{
//  for(unsigned int i=0;i<traj.size();i++)
//    traj[i] = MathHelper::rad2deg(traj[i]);
//}

