
#ifndef TRAJECTORIES_TRAJHELPER_H_
#define TRAJECTORIES_TRAJHELPER_H_

#include "TypeDefinitions.h"
#include <vector>

using namespace ourTypeDefinitions;

class TrajHelper
{
  public:
	  static void confVectorToFile(const char* fileName, const std::vector<QVector7> &thePoints, bool toDegrees);
	  static void confDataToFile(const char* fileName, const std::vector<QVector7>& torques, const std::vector<QVector7>& posPoints, const std::vector<QVector7>& velPoints, bool toDegrees);
	  static void rad2deg(std::vector<QVector7> &traj);
};

#endif /* TRAJECTORIES_TRAJHELPER_H_ */
