#pragma once
#include <vector>

class TrajP
{
protected:
  double samplingTime;

public:
  TrajP(double sampling_time) :samplingTime(sampling_time){};
  virtual double getQ(unsigned int i)=0;
  virtual ~TrajP(){}
  virtual int getNumSamples() = 0;
  double getSampligTime() { return samplingTime; }
};
